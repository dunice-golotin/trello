import { Component, OnInit } from '@angular/core';
import { BoardService} from './service/board.service'
import { Response } from '@angular/http'
import { CreateWindowService } from './service/createwindow.service'
import { Card } from './classes/card.class';
import { Board } from './classes/board.class'
import {AuthenticationService} from './service/authentication.service';
import { Subject } from 'rxjs/Subject'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:  [BoardService]
})
export class AppComponent implements OnInit{
  icon:string;
  user: string;
  create: boolean;

  constructor(private appService: BoardService,
  private createWindowService:CreateWindowService,
  private authenticationService:AuthenticationService) { }
  ngOnInit() {
    this.create=false;
    if(JSON.parse(localStorage.getItem('currentUser'))){
      this.user = JSON.parse(localStorage.getItem('currentUser')).username
      this.icon = this.user[0];
    }
    else {
      this.user = '';
      this.icon = '';
    }
    this.authenticationService.username.subscribe((value)=>{this.user=value; this.icon=value[0]})
    //this.icon=this.user;
  }
  
  createNewBoard(inf){
    this.appService.createNewBoard(inf).subscribe((data) =>{})
  }
  createLogOut(){
    this.createWindowService.logOutWindow();
  }
}
