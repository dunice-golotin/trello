import { Component, OnInit, OnDestroy ,EventEmitter, Output, Input, ApplicationRef, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalWindowService } from '../../service/modal-window.service'
import { Location } from '@angular/common';
import { List } from '../../classes/list.class'
import { Card } from '../../classes/card.class';
import {NgbModal, NgbModalOptions, NgbActiveModal,  ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-modalwindow',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.css'],
})
export class ModalWindowComponent implements OnInit {
  name: string;
  @Input() link: Card;
  parent: List;
  editDiscrib: boolean;
  user: string;
  constructor(private activateRoute: ActivatedRoute,
              public activeModal: NgbActiveModal, 
              public changeRef: ChangeDetectorRef, 
              private modalWindowService: ModalWindowService,
              private location: Location) { 
  }

  ngOnInit() {
    this.parent=new List()
    this.getParent(this.link)
    this.editDiscrib=false;
    if(JSON.parse(localStorage.getItem('currentUser'))){
      this.user = JSON.parse(localStorage.getItem('currentUser')).username[0]
    } 
    else{
      this.user = ''
    }
  }
  ngOnDestroy(){
    this.modalWindowService.saveChanges(this.link.value,this.link.id).subscribe();
  }
  goBack():void{
    this.activeModal.close()
  }

  saveDiscibtion(value:string){
    this.link.description=value;
    this.modalWindowService.saveDescription(this.link.id,value).subscribe()
  }
  
  sendComment(value:string){
    if(value==undefined || value.length==0){
      return;
    }
    this.modalWindowService.sendComment(this.link,value).subscribe();
  }

  getParent(data:Card){
    this.modalWindowService.findParent(data.parentId).subscribe((data)=>{this.parent=data[0]})
  }
}
