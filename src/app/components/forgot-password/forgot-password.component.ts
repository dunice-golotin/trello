import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../service/authentication.service';
import { PasswordResetService } from '../../service/password-reset.serivce';
@Component({

    selector: 'forgot-password',
    templateUrl: 'forgot-password.component.html',
    styleUrls: ['forgot-password.component.css']
})
 
export class ForgotPasswordComponent implements OnInit {
    mail:string;
    errorLog: string='';
 
    constructor(private passwordResetService:PasswordResetService,
                private authenticationService: AuthenticationService) { }
 
    ngOnInit() {
       this.authenticationService.logOut();
    }
    resetPassword(){
        this.passwordResetService.resetPassowrd(this.mail)
        .subscribe((res)=>{this.errorLog='The information about change your password in your mail';}
        ,(err)=>{this.errorLog=err.message,console.log(err)});
    }
}