import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PasswordResetService } from '../../service/password-reset.serivce';
import { Router} from '@angular/router';
import { AuthenticationService } from '../../service/authentication.service';
@Component({
    selector: 'app-password-reset',
    templateUrl: './password-reset.component.html',
    styleUrls: ['./password-reset.component.css'],
})
export class PasswordResetComponent implements OnInit{
    token:string;
    password:string;
    displayErr:string='';
    constructor(private activatedRoute:ActivatedRoute,
                private passwordResetService: PasswordResetService,
                private route: Router,
                private authenticationService:AuthenticationService){
    }
    ngOnInit(){
        this.activatedRoute.url.subscribe((url)=>{this.token=url[1].path})
        this.authenticationService.logOut();
    }
    changePassword(){
        this.passwordResetService
        .changePassword(this.token,this.password)
        .subscribe((value)=>{
            this.route.navigate(['/login'])},
            (err)=>{console.log(err), this.password=''}
            ) 
    }
}