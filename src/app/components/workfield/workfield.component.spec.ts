import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { workfieldComponent } from './workfield.component';

describe('workfieldComponent', () => {
  let component: workfieldComponent;
  let fixture: ComponentFixture<workfieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ workfieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(workfieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
