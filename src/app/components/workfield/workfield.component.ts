import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { List } from '../../classes/list.class';
import { Card } from '../../classes/card.class';
import { WorkfieldService } from '../../service/workfield.service';
import { Board } from '../../classes/board.class';
import { ListComponent } from '../list/list.component';
import { DragulaService } from 'ng2-dragula'

import {Renderer,} from '@angular/core'
@Component({
  selector: 'app-workfield',
  templateUrl: './workfield.component.html',
  styleUrls: ['./workfield.component.css'],
  providers: [WorkfieldService]
})
export class WorkfieldComponent implements OnInit {
  name:string;
  uniqueBagId: string =''+(+new Date);
  lists: Array<List>;
  inputactive: boolean;
  link:Board=new Board();
  st_list: boolean = false;
  constructor(private activateRoute: ActivatedRoute,
              private workFieldService: WorkfieldService,
              private dragulaService: DragulaService,
              private renderer:Renderer) { 
      dragulaService.setOptions(this.uniqueBagId,{
        moves: function(el:any, container: any, handle: any):any{
          return handle.className === 'handle' 
        }
      })
      dragulaService.dropModel.subscribe((value)=>this.onDropModel(value.slice(1)));
      
  }
  private onDropModel(args) {
    let [el, target, source] = args;
    this.renderer.selectRootElement(el) 
    let obj=new Object();
    for (let i in this.lists){
      obj[this.lists[i].position]=+i;
      this.lists[i].position=+i;
    }
    this.workFieldService.updateDragAndDrop(this.name,obj)
        .subscribe((res)=>{this.lists=res;this.lists.sort(this.sortByposition)})
  }
  ngOnInit() {
     this.inputactive=false;
     this.activateRoute.url.subscribe((url)=>{this.name=url[1].path});
     this.lists=[];
     this.workFieldService.findworkfieldData(this.name)
        .subscribe((data) => {this.lists=data;this.lists.sort(this.sortByposition)});
     this.workFieldService.findLink(this.name).subscribe((data)=>{this.link=data});
  }
  ngOnDestroy(){

  }
  deleteList(object:List){
    this.workFieldService.listDelete(object.id,object.position,object.parentId)
        .subscribe((data)=>{this.lists.splice(this.searcElement(data),1)})
  }
  newList(event){
    this.inputactive=false;
    if(event==undefined || event==''){
     
      return
    }
    let position = this.lists.length;
    this.workFieldService.AddNewLis(event,this.name,position)
        .subscribe((data)=>{this.lists.push(data);});
  }
  searcElement(id):number{
    for(let i in this.lists){
      if(this.lists[i].id==id.id){
        return +i;
      }
    }
  }
  sortByposition(a:List,b:List){
    if (a.position>b.position)
    {
      return 1;
    }
    else{
      return -1;
    }
  }
  updateName(){
    this.workFieldService.updateName(this.link.id,this.link.name).subscribe((doc)=>{})
  }
  close(){
    setTimeout(()=>this.inputactive=false,100)
  }
}