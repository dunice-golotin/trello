import { Component, OnInit, Input } from '@angular/core';
import { Comment } from '../../classes/card.class'
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input() comment: Comment;
  user:string;
  icon:string;
  constructor() { }

  ngOnInit() {

    this.user=this.comment.user;
    this.icon=this.user[0]
  }

}
