import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetBoardComponent } from './setboard.component';

describe('TableComponent', () => {
  let component: SetBoardComponent;
  let fixture: ComponentFixture<SetBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
