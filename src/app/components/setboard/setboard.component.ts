import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './setboard.component.html',
  styleUrls: ['./setboard.component.css']
})
export class SetBoardComponent implements OnInit {
  
  name:string;
  @Output() something = new EventEmitter<string>();
  @Output() back = new EventEmitter<boolean>();
  change(increased) {
        this.something.emit(increased);
  }
  constructor() { }
 
  send(name){
    this.something.emit(name);
    this.name=''
  }
  callBack(){
    this.back.emit(false);
  }
  ngOnInit() {
    
  }
}
