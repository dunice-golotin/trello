import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-setlist',
  templateUrl: './setlist.component.html',
  styleUrls: ['./setlist.component.css']
})

export class SetListComponent implements OnInit {
  name:string;
  @Output() something = new EventEmitter<string>();
  @Output() back = new EventEmitter<boolean>();
  change(increased) {
        this.something.emit(increased);
  }
  constructor() { }
  send(evename){
    this.something.emit(evename);
    this.name=''
  }
  callBack(){
    this.back.emit(false);
  }
  ngOnInit() {
  }
  close(){
    this.back.emit(false)
  }
}
