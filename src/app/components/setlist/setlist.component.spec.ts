import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardaddComponent } from './boardadd.component';

describe('BoardaddComponent', () => {
  let component: BoardaddComponent;
  let fixture: ComponentFixture<BoardaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
