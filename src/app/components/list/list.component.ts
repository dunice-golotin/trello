import { Component, OnInit, Input, AfterViewInit, ViewChild ,ElementRef} from '@angular/core';
import { List } from '../../classes/list.class'
import { Card } from '../../classes/card.class'
import { ListService } from '../../service/list.service';
import { DragulaService } from 'ng2-dragula'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  @Input() list: List;
  newCard: boolean = false;
  card:Array <Card>;
  edintName: boolean;
  cardName:string;
  constructor(private listService: ListService, 
  private dragulaService: DragulaService, 
  ) { 
    dragulaService.dropModel
        .subscribe((value)=>this.onDropModel(value.slice(1)));
    this.edintName=false;
  }
  private onDropModel(args) {
    let [el, target, source] = args;
    let id :string;
    let newposition: number;
    let obj=new Object();
    for(let card in this.card){
      if(this.list.id!=this.card[card].parentId){
        id=this.card[card].id,
        newposition=+card;
        this.card[card].parentId=this.list.id
        this.card[card].position=+card;
      }
      else{
         obj[this.card[card].id]=+card;
         this.card[card].position=+card;
      }
    }
    this.listService.cardDragAndDrop(this.list.id,obj,id,newposition).subscribe((res)=>{});
  }
  getCard(){
     this.listService.findCard(this.list.id)
        .subscribe((data)=>{this.card=data.sort(this.sortByPosition)});
  }
  ngOnInit() {
    this.card=[];
    this.getCard()
  }

  addNewCard(cardname:string=''){
    if(cardname==''){
      return;
    }
    let position = this.card.length;
    this.listService.addNewCard(cardname,this.list,position)
        .subscribe((data)=>(this.card.push(data)));
    this.cardName="";
  }
  closeWindow(){
    setTimeout(()=>{this.cardName='';this.edintName=false},300)
  }
  sortByPosition(a:Card,b:Card){
    if (a.position>b.position)
    {
      return 1;
    }
    else{
      return -1;
    }
  }
  changeListName(name:string='',id:string){
    if(name.length==0){
      return;
    }
    this.listService.changeListName(name,id).subscribe((data)=>{this.list.name=data.name});
  }
 
}
