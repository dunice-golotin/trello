import { Component, OnInit, Input } from '@angular/core';
import { Card } from '../../classes/card.class'
import { CardService} from '../../service/card.service'
import { CreateWindowService } from '../../service/createwindow.service'
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  @Input() card : Card;
  edit: boolean;
  constructor(private cardService:CardService,
  private createWindowService: CreateWindowService,
  ) { }

  ngOnInit() {
    this.edit=false;
  }
  updateCard(){
    this.cardService.updateCard(this.card.id,this.card.value).subscribe()
  }
  createModalWindow(){
    this.createWindowService.diplayWindow(this.card);
  }
}
