import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable} from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { SearchService } from '../../service/search.service';
import { Board } from '../../classes/board.class'
// Observable class extensions
import 'rxjs/add/observable/of';

// Observable operators
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [SearchService]
})
export class SearchComponent implements OnInit {
  private searchTerms= new Subject<string>();
  board: Observable<Board[]>;
  boards: Array<Board>;
  items: Array<string>;
  constructor(private router:Router,
              private searchService: SearchService
  ) { 
    
  }
  ngOnInit() {
  }
  search(term:string){
    if(term==undefined || term.length==0){
      return;
    }
    this.searchService.search(term).subscribe((value)=>{this.boards=value})
  }
  reset(term){
    setTimeout(()=>{this.boards=undefined},300)
  }
}
