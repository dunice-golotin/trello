import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { BoardService } from '../../service/board.service';
import { Board } from '../../classes/board.class';
import { WorkfieldComponent} from '../workfield/workfield.component'
import { List } from '../../classes/list.class'
import { UserService } from '../../service/user.service';
import { User } from '../../classes/user.class';
import { AppComponent } from '../../app.component'
@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css'],
})
export class BoardsComponent implements OnInit {
  id: string;
  boards: Array <Board>;
  flagInput:boolean;
  users: Array<User>=[];
  constructor(private appService: BoardService,
  private userService: UserService) {  
  }

  ngOnInit(id:string="panelboards") {
    this.id=id;
    this.flagInput=false;
    this.getBoard();
    this.appService.newBoard.subscribe((val)=>{this.boards.push(val)})
  }

  ngOnDestroy(){
  }

  addNewBoard(inf:string = ''){
    if(inf.length==0){
      return;
    }
    this.appService.createNewBoard(inf).subscribe((data) => {}) 
  }
  getBoard(){
    this.appService.getBoards().subscribe(boards=>this.boards=boards)
  }

  callBack(event){
    this.flagInput=false;
  }
}