import {Component, OnInit} from '@angular/core';
import {RegistrationService} from '../../service/registration.service'
import {Router} from '@angular/router';
import { AuthenticationService } from '../../service/authentication.service';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
    model:any={};
    displayErr: string;
    constructor(private registrationService:RegistrationService,
    private router: Router,
    private authenticationService: AuthenticationService){
    }
    ngOnInit(){
        this.displayErr='';
        this.authenticationService.logOut();
    }
    registration(){
        this.registrationService.registration(this.model.username,this.model.password,this.model.mail)
        .subscribe(
            (resp)=>this.router.navigate(['/login']),
            (err)=>{
                this.model.username='';
                this.model.password='';
                this.model.mail=''
                return this.displayErr=err.message
            })
    }
}