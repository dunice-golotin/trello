import { Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {NgbModal, NgbModalOptions, NgbActiveModal,  ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {AuthenticationService} from '../../service/authentication.service'
@Component({
  selector: 'app-log-out',
  templateUrl: './log-out-modal.component.html',
  styleUrls: ['./log-out-modal.component.css'],
})
export class LogOutModal implements OnInit {
    constructor(public activeModal: NgbActiveModal, 
    public changeRef: ChangeDetectorRef,
    private authenticationService:AuthenticationService ) { }
 
    ngOnInit() {
        
    }
    closeModal(){
        this.activeModal.close();
    }
    logOut(){
        this.authenticationService.logOut();
        this.activeModal.close();
    }
}