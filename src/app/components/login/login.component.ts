import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
 
import { AuthenticationService } from '../../service/authentication.service';
 
@Component({

    selector: 'app-registration',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']
})
 
export class LoginComponent implements OnInit {
    model: any = {};
    loading:boolean = false;
    errorLog: string;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService) { }
 
    ngOnInit() {
        this.authenticationService.logOut();
        this.errorLog='';
    }
 
    login() {
        if(!this.model.username || !this.model.password)
        {
            return;
        }
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
                (result) => {
                    this.router.navigate(['/']);},
                (err) => {
                    this.errorLog = (`this user doesn't exist`)
                }
                    );
        this.model.password='';
        this.model.username='';
    }
}