import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component'
import { BoardsComponent }   from './components/boards/boards.component';
import { WorkfieldComponent } from './components/workfield/workfield.component';
import { LoginComponent} from './components/login/login.component';
import { AuthGuard} from './guards/auth.guard';
import { RegistrationComponent } from './components/registration/registration.component';
import { PasswordResetComponent } from './components/password-reset/password-reset.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
const routes: Routes = [
  //{ path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'boards',  component: BoardsComponent, canActivate:[AuthGuard] },
  { path: 'workfield/:id', component: WorkfieldComponent,  canActivate:[AuthGuard]},
  { path: 'registration',component: RegistrationComponent},
  { path: 'reset/:param', component: PasswordResetComponent},
  { path: 'forgot-passowrd', component:ForgotPasswordComponent},
  { path: '**', redirectTo: 'boards'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}