import {Card} from './card.class'
export class List{
    parentId: string;
    id: string;
    name: string;
    value: Array<Card>;
    position: number;
};