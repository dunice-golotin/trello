export class Card{
    parentId: string;
    id: string;
    value: string;
    comments: Array<Comment>=[];
    description: string;
    position:number;
}
export class Comment{
    message: string;
    user: string;
}