import {List} from './list.class'


export class Board{
    id: string;
    name: string;
    data: Array<List>;
}