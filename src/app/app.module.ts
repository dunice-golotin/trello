import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule , JsonpModule} from '@angular/http';
import { RouterModule, Routes } from '@angular/router'
import { AppComponent } from './app.component';
import { BoardsComponent } from './components/boards/boards.component';
import { AppRoutingModule } from './app.routing';
import { SetBoardComponent } from './components/setboard/setboard.component';
import { WorkfieldComponent } from './components/workfield/workfield.component';
import { ListComponent } from './components/list/list.component';
import { CardComponent } from './components/card/card.component';
import { DragulaModule } from 'ng2-dragula';
import { ModalWindowComponent } from './components/modal-window/modal-window.component';
import { SetListComponent } from './components/setlist/setlist.component';
import { CommentComponent } from './components/comment/comment.component';
import { SearchComponent } from './components/search/search.component';
import { NgbModule, NgbModal,  } from '@ng-bootstrap/ng-bootstrap' 
import { CreateWindowService} from './service/createwindow.service';
import { APP_BASE_HREF } from '@angular/common';
import { BoardService} from './service/board.service';
import { ListService } from './service/list.service'
import { ModalWindowService } from './service/modal-window.service';
import { CardService } from './service/card.service';
import { RegistrationComponent } from './components/registration/registration.component';
import { AuthGuard } from './guards/auth.guard';
import { AuthenticationService } from './service/authentication.service';
import { UserService } from './service/user.service'
import { LoginComponent } from './components/login/login.component';
import { RegistrationService } from './service/registration.service'
import { LogOutModal} from './components/log-out-modal/log-out-modal.component';
import { PasswordResetComponent } from './components/password-reset/password-reset.component';
import { PasswordResetService } from './service/password-reset.serivce';
import { ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';

import {FocusInput} from './directives/focus.directive';
const routes: Routes = [
  //{ path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'boards',  component: BoardsComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    BoardsComponent,
    SetBoardComponent,
    WorkfieldComponent,
    ListComponent,
    CardComponent,
    ModalWindowComponent,
    LogOutModal,
    SetListComponent,
    CommentComponent,
    SearchComponent,
    LoginComponent,
    RegistrationComponent,
    PasswordResetComponent,
    ForgotPasswordComponent,
    FocusInput,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    DragulaModule,
    JsonpModule,
    NgbModule.forRoot(),
  ],
  providers: [CreateWindowService,
              BoardService,
              ListService,
              ModalWindowService,
              CardService,
              AuthGuard,
              AuthenticationService,
              UserService,
              RegistrationService,
              PasswordResetService],
  bootstrap: [AppComponent],
  entryComponents: [ModalWindowComponent,LogOutModal]
})
export class AppModule { }
