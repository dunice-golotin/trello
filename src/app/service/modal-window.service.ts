import { Injectable } from '@angular/core';
import { List } from '../classes/list.class'
import { Card } from '../classes/card.class'
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http'
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from './authentication.service'
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
@Injectable()
export class ModalWindowService {

  constructor(private http: Http,
  private authenticationService: AuthenticationService) { }
  serverUrl:string = 'http://127.0.0.1:3000/modal-window/'
  saveDescription(id:string, description: string){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .post(this.serverUrl+'description',{id:id, description:description},options)
                .map((data)=>data.json())
                .catch((err)=>Observable.throw('err'))
  }
  sendComment(parent:Card,comment:string){
    if(comment.trim()==''){
      return;
    }
    parent.comments.push({message:comment,user:JSON.parse(localStorage.getItem('currentUser')).username})
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .post(this.serverUrl+'comment',{id:parent.id, value:parent.comments},options)
                .debounceTime(300)
                .map((data)=>data.json())
                .catch((err)=>Observable.throw('err'))
  }
  findParent(parentId:string){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .get(this.serverUrl+'list'+`/?parentId=${parentId}`,options)
                .map((data)=>data.json())
                .catch((err)=>Observable.throw('err'))
    
  }
  saveChanges(value:string, id: string){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .post(this.serverUrl+'update',{value:value,id:id},options)
                .map((data)=>data.json())
                .catch((err)=>Observable.throw('err'))
  }
  
}
