import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable'
import { Http, RequestOptions,Headers } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
@Injectable()
export class PasswordResetService {
    constructor(private http: Http){};
    changePassword(token:string,password:string){
        return this.http
                    .post('http://127.0.0.1:3000/reset-password/password',{token:token,password:password})
                    .map((res)=>res.json())
                    .catch((err)=>Observable.throw(err));
    }
    resetPassowrd(mail:string){
        return this.http
                    .post('http://127.0.0.1:3000/reset-password/reset',{mail:mail}).map((res)=>res.json()).catch((err)=>Observable.throw(err.json()));
    }
}