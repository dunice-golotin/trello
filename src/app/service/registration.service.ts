import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable'
import { Http, URLSearchParams, Jsonp, RequestOptions,Headers } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
@Injectable()
export class RegistrationService {
    constructor(private http: Http){};
    registration(username:string, password:string, mail: string){
        return this.http
        .post('http://127.0.0.1:3000/users',{name: username, password: password, mail:mail})
        .map((res)=>res.json()).catch((err)=>{return Observable.throw(err.json())})
    }
}