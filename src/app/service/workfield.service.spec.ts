import { TestBed, inject } from '@angular/core/testing';

import { BoardworkfieldService } from './workfield.service';

describe('BoardworkfieldService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BoardworkfieldService]
    });
  });

  it('should ...', inject([BoardworkfieldService], (service: BoardworkfieldService) => {
    expect(service).toBeTruthy();
  }));
});
