import { Injectable } from '@angular/core';
import { Http, URLSearchParams, Jsonp, RequestOptions,Headers } from '@angular/http';
import { AuthenticationService} from './authentication.service'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
//import { Board } from '../components/boards/boards.array'
@Injectable()
export class SearchService {

  constructor(private http:Http,
  private authenticationService: AuthenticationService) { }
  search(term:string){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`http://127.0.0.1:3000/board/search/?id=${term}`,options).map((res)=>res.json())
  }
}
