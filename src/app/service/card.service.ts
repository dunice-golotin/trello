import { Injectable } from '@angular/core';
import { List } from '../classes/list.class'
import { Card } from '../classes/card.class'
import { ListComponent} from '../components/list/list.component';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService} from './authentication.service'
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
@Injectable()
export class CardService{
    constructor(private http: Http,
    private authenticationService: AuthenticationService){}
    
    updateCard(id:string, value: string){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .post('http://127.0.0.1:3000/list/update', { id:id, value:value})
                .map((res)=>res.json())
                .catch((err)=>Observable.throw('err'))
  }
}