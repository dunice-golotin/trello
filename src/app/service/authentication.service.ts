import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { Subject} from 'rxjs/Subject'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
@Injectable()
export class AuthenticationService {
    public token: string;
    public username : Subject <string>  = new Subject();
    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;

    }
 
    login(username: string, password: string): Observable<boolean> {
        return this.http.post('http://127.0.0.1:3000/token', { name: username, password: password })
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                // set token property
                this.token = 'JWT '+token;
                this.username.next(username)
                // store username and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify({ username: username, token: this.token }));    

            }).catch((err)=>{return Observable.throw(err)});
    }

    logOut(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}