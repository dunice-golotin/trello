import { Injectable } from '@angular/core';
import { Board} from '../classes/board.class';
import { List } from '../classes/list.class'
import { Card } from '../classes/card.class'
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject'
import { AuthenticationService } from './authentication.service'
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
@Injectable()
export class BoardService {
  private echourl = 'http://127.0.0.1:3000/board';
  public newBoard = new Subject <Board>(); 
  constructor(private http: Http,
  private authenticationService:AuthenticationService) {
  }
  
  createNewBoard(name:string=''){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .post(this.echourl,{name:name, id:name+(+new Date())},options)
                .map((res)=>{this.newBoard.next(res.json());return res.json()})
                .catch((err)=> Observable.throw('err'))
  }
  getBoards(){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.echourl,options)
                .map((res)=>res.json())
                .catch((err)=> {console.log(err);return Observable.throw('err')})
  }
}
