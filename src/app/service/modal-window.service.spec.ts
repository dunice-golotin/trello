import { TestBed, inject } from '@angular/core/testing';

import { ModalWindowService } from './modal-window.service';

describe('ModalwindowService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModalWindowService]
    });
  });

  it('should ...', inject([ModalWindowService], (service: ModalWindowService) => {
    expect(service).toBeTruthy();
  }));
});
