import { Injectable } from '@angular/core';
import { List } from '../classes/list.class'
import { Card } from '../classes/card.class'
import { ListComponent} from '../components/list/list.component';
import { Http, Response, Headers, RequestOptions, RequestMethod, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService} from './authentication.service'
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
@Injectable()
export class ListService{
  private serverUrl='http://127.0.0.1:3000/list/'
  constructor(private http: Http,
  private authenticationService:AuthenticationService) { }

  findCard(id:string){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .get(this.serverUrl+'find'+`/?id=${id}`,options)
                .map((res)=>res.json())
                .catch((err)=>Observable.throw('err'))
  }
  addNewCard(value:string, boardlist: List, position: number){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .post(this.serverUrl+'add',{value:value, boardlist:boardlist, position: position},options)
                .map((res)=>res.json())
                .catch((err) => Observable.throw('err'))
  }
  cardDragAndDrop(parentId:string,obj:Object,id:string,newposition:number){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .post(this.serverUrl+'drag',{ parentId:parentId, obj: obj, id:id, newposition:newposition},options)
                .map((card)=>{return card.json()})
                .catch((err)=>Observable.throw('err'))
  }
  changeListName(name:string, id: string){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .post(`http://127.0.0.1:3000/workfield/listname`,{id:id, name:name},options)
                .map((res)=>res.json())
                .catch((err)=>Observable.throw(err))
  }
}