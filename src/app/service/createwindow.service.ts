import { Injectable }    from '@angular/core';
import { Observable }    from 'rxjs/Observable';
import { Card } from '../classes/card.class'
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import {NgbModal, NgbModalOptions, NgbActiveModal,  ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Component, Input, OnInit, ApplicationRef, ChangeDetectorRef} from '@angular/core';
import {LogOutModal} from '../components/log-out-modal/log-out-modal.component';
import {ModalWindowComponent } from '../components/modal-window/modal-window.component'
@Injectable()
export class CreateWindowService {

  constructor(private modalService: NgbModal) { }
  diplayWindow(card:Card){
      const modalRef = this.modalService.open(ModalWindowComponent);
      modalRef.componentInstance.link=card;
      return modalRef.result
  }
  logOutWindow(){
      const modalRef = this.modalService.open(LogOutModal);
  }
}