import { Injectable } from '@angular/core';
import { List } from '../classes/list.class'
import { Card } from '../classes/card.class'
import { ListComponent} from '../components/list/list.component';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService} from './authentication.service'
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
@Injectable()
export class WorkfieldService {

  private serverUrl='http://127.0.0.1:3000/workfield'

  constructor(private http: Http, private authenticationService:AuthenticationService) { }
  AddNewLis(name:string, parentId:string, position:number){
    if(name.trim()==''){
      return;
    }
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .post(this.serverUrl+'/add', {name:name,link:parentId,position:position},options)
                .map((res)=>res.json())
                .catch((err)=>Observable.throw(err))
  }
  
  listDelete(id:string,position:number,parentId:string){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    return this.http
                .delete(this.serverUrl+'/delete',{body:{id:id,position:position,parentId:parentId},headers:headers})
                .map((res)=>res.json())
                .catch((err)=>Observable.throw(err))

  }
  findworkfieldData(id:string){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .get(`http://127.0.0.1:3000/workfield/?id=${id}`,options)
                .map((res)=>res.json())
                .catch((err) => Observable.throw('err'))
  }
  
  
  updateDragAndDrop(parentId:string, dragelem:Object){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .post(this.serverUrl+'/drag',{id:parentId, value:dragelem},options)
                .map((res)=>res.json())
                .catch((err)=>Observable.throw('err'))
  }
  findLink(id:string){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .get(`http://127.0.0.1:3000/workfield/link/?id=${id}`, options)
                .map((res)=>res.json())
                .catch((err)=>Observable.throw(err))
  }
  updateName(id:string, value:string){
    let headers = new Headers({ 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http
                .post(`http://127.0.0.1:3000/workfield/link`,{id:id,value:value},options)
                .map((res)=>res.json())
                .catch((err)=>Observable.throw(err))
  }
}
